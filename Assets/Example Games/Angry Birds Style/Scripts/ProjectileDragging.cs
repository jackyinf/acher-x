﻿using System;
using UnityEngine;
// ReSharper disable UnusedMember.Local

public delegate void OnFired(object sender, OnFiredArgs args);

public class OnFiredArgs
{
}


public class ProjectileDragging : MonoBehaviour {
	public float maxStretch = 3.0f;
	public LineRenderer catapultLineFront;
	public LineRenderer catapultLineBack;  
	
	private SpringJoint2D spring;
	private Transform catapult;
	private Ray rayToMouse;
	private Ray leftCatapultToProjectile;
	private float maxStretchSqr;
	private float circleRadius;
	private bool clickedOn;
	private Vector2 prevVelocity;
	
    public event OnFired OnFired;
    public bool isConnected = false;

    public void Connect(Rigidbody2D other, LineRenderer lineFront, LineRenderer lineBack)
    {
        try
        {
            spring = GetComponent<SpringJoint2D>();
            spring.connectedBody = other;
            catapult = spring.connectedBody.transform;
            isConnected = true;

            catapultLineFront = lineFront;
            catapultLineBack = lineBack;
            DoMathStuff();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
        }
    }

	void Awake ()
    {
	    try
	    {
	        spring = GetComponent <SpringJoint2D> ();
	        catapult = spring.connectedBody.transform;
	        isConnected = true;
	    }
	    catch
	    {
	        isConnected = false;
	    }
	}
	
	void DoMathStuff ()
    {
        if (!isConnected)
            return;

        //LineRendererSetup ();
        rayToMouse = new Ray(catapult.position, Vector3.zero);
		leftCatapultToProjectile = new Ray(catapultLineFront.transform.position, Vector3.zero);
		maxStretchSqr = maxStretch * maxStretch;
		CircleCollider2D circle = GetComponent<Collider2D>() as CircleCollider2D;
		circleRadius = circle.radius;
	}
	
	void Update ()
	{
	    if (!isConnected)
	        return;

		if (clickedOn)
			Dragging ();
		
		if (spring != null) {
			if (!GetComponent<Rigidbody2D>().isKinematic && prevVelocity.sqrMagnitude > GetComponent<Rigidbody2D>().velocity.sqrMagnitude) {
				Destroy (spring);
				GetComponent<Rigidbody2D>().velocity = prevVelocity;
			}
			
			if (!clickedOn)
				prevVelocity = GetComponent<Rigidbody2D>().velocity;
			
			//LineRendererUpdate ();
			
		} else {
			catapultLineFront.enabled = false;
			catapultLineBack.enabled = false;
		}
	}
	
	void LineRendererSetup ()
    {
		catapultLineFront.SetPosition(0, catapultLineFront.transform.position);
		catapultLineBack.SetPosition(0, catapultLineBack.transform.position);
		
		catapultLineFront.sortingLayerName = "Foreground";
		catapultLineBack.sortingLayerName = "Foreground";
		
		catapultLineFront.sortingOrder = 3;
		catapultLineBack.sortingOrder = 1;
	}
	
	void OnMouseDown ()
    {
	    if (!isConnected)
	    {
            Debug.LogFormat("Projectile's joint is not connected");
            return;
        }

        spring.enabled = false;
		clickedOn = true;
	}
	
	void OnMouseUp ()
	{
	    if (OnFired != null)
	        OnFired(this, new OnFiredArgs());

        spring.enabled = true;
		GetComponent<Rigidbody2D>().isKinematic = false;
		clickedOn = false;
	}

	void Dragging ()
    {
		Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector2 catapultToMouse = mouseWorldPoint - catapult.position;
		
		if (catapultToMouse.sqrMagnitude > maxStretchSqr) {
			rayToMouse.direction = catapultToMouse;
			mouseWorldPoint = rayToMouse.GetPoint(maxStretch);
		}
		
		mouseWorldPoint.z = 0f;
		transform.position = mouseWorldPoint;
	}

	void LineRendererUpdate ()
    {
		Vector2 catapultToProjectile = transform.position - catapultLineFront.transform.position;
		leftCatapultToProjectile.direction = catapultToProjectile;
		Vector3 holdPoint = leftCatapultToProjectile.GetPoint(catapultToProjectile.magnitude + circleRadius);
		catapultLineFront.SetPosition(1, holdPoint);
		catapultLineBack.SetPosition(1, holdPoint);
	}
}