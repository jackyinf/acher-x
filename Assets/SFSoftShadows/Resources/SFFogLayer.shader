﻿Shader "Hidden/SFSoftShadows/FogLayer" {
	Properties {
		_FogColor ("Fog color and alpha.", Color) = (1.0, 1.0, 1.0, 0.0)
		_Scatter ("Light scattering color (RGB), Hard/soft mix (A)", Color) = (1.0, 1.0, 1.0, 0.15)
	}
	
	SubShader {
		Pass {
			Blend One OneMinusSrcAlpha
			Cull Off
			Lighting Off
			ZTest Always
			ZWrite Off
			
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex VShader
				#pragma fragment FShader
				
				sampler2D _SFLightMap;
				sampler2D _SFLightMapWithShadows;
				
				// Transmi
				half4 _FogColor;
				half4 _Scatter;
				
				struct VertexInput {
					float3 position : POSITION;
					float2 texCoord : TEXCOORD0;
				};
				
				struct VertexOutput {
					float4 position : SV_POSITION;
					float2 lightCoord : TEXCOORD1;
				};
				
				VertexOutput VShader(VertexInput v){
					float4 position = mul(UNITY_MATRIX_MVP, float4(v.position, 1.0));
					VertexOutput o = {position,0.5*position.xy/position.w + 0.5};
					return o;
				}
				
				fixed4 FShader(VertexOutput v) : SV_Target {
					half3 l0 = tex2D(_SFLightMap, v.lightCoord).rgb;
					half3 l1 = tex2D(_SFLightMapWithShadows, v.lightCoord).rgb;
					half3 scatter = _Scatter.rgb*lerp(l0, l1, _Scatter.a);
					
					return half4(_FogColor.rgb*_FogColor.a + scatter, _FogColor.a);
				}
			ENDCG
		}
	}
}
