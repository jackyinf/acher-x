﻿Shader "Sprites/SFSoftShadow" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SoftHardMix ("Unshadowed/Shadowed Mix", Range(0.0, 1.0)) = 0.0
		_Glow ("Self Illumination", Color) = (0.0, 0.0, 0.0, 0.0)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}
	
	SubShader {
		Tags {
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}
		
		Pass {
			Blend One OneMinusSrcAlpha
			Cull Off
			Lighting Off
			ZWrite Off
			
			Fog {
				Mode Off
			}
			
			CGPROGRAM
				#include "UnityCG.cginc"
				#pragma vertex VShader
				#pragma fragment FShader
				#pragma multi_compile _ PIXELSNAP_ON
				
				sampler2D _MainTex;
				float4 _MainTex_ST;
				
				sampler2D _SFLightMap;
				sampler2D _SFLightMapWithShadows;
				float _SFGlobalDynamicRange;
				
				float _SoftHardMix;
				float4 _Glow;
				
				struct VertexInput {
					float3 position : POSITION;
					float2 texCoord : TEXCOORD0;
					float4 color : COLOR;
				};
				
				struct VertexOutput {
					float4 position : SV_POSITION;
					float2 texCoord : TEXCOORD0;
					float2 lightCoord : TEXCOORD1;
					float4 color : COLOR;
				};
				
				VertexOutput VShader(VertexInput v){
					float4 position = mul(UNITY_MATRIX_MVP, float4(v.position, 1.0));
					
					#if defined(PIXELSNAP_ON)
					position = UnityPixelSnap(position);
					#endif
					
					VertexOutput o = {
						position,
						TRANSFORM_TEX(v.texCoord, _MainTex),
						0.5*position.xy/position.w + 0.5,
						v.color,
					};
					return o;
				}
				
				fixed4 FShader(VertexOutput v) : SV_Target {
					fixed4 color = v.color*tex2D(_MainTex, v.texCoord);
					
					fixed3 l0 = tex2D(_SFLightMap, v.lightCoord).rgb;
					fixed3 l1 = tex2D(_SFLightMapWithShadows, v.lightCoord).rgb;
					fixed3 light = lerp(l0, l1, _SoftHardMix) + _Glow;
					color.rgb *= light*_SFGlobalDynamicRange*color.a;
					
					return color;
				}
			ENDCG
		}
	}
}
