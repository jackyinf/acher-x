﻿Shader "Custom/ 2 - Lambert" {
	Properties {
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
	}
	SubShader {
		Pass {
			Tags { "LightMode" = "ForwardBase" }	// special info about each pass. Here, it allows you to detect where direction light is pointed
CGPROGRAM

	#pragma vertex vert
	#pragma fragment frag

	uniform float4 _Color;

	uniform float4 _LightColor0;

	// In Unity 4+ these are predefined
	//float4x4 _Object2World;
	//float4x4 _World2Object;
	//float4 _WorldSpaceLightPos0

	// base input structs
	struct vertexInput {
		float4 vertex: POSITION;
		float3 normal: NORMAL;
	};
	struct vertexOutput {
		float4 pos: SV_POSITION;
		float4 col: COLOR;
	};

	// vertex function
	vertexOutput vert(vertexInput v) {
		vertexOutput o;

		// normalize squeezes any values between 0 and 1
		float3 normalDirection = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
		float3 lightDirection;
		float atten = 1.0;

		lightDirection = normalize(_WorldSpaceLightPos0.xyz);

		// max trunks off everything below zero
		// _LightColor0.xyz is direction lights color
		float3 diffuseReflection = atten * _LightColor0.xyz * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

		o.col = float4(diffuseReflection, 1.0);	// 4th component - 1.0 - is aplha
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		return o;
	}

	// fragment function
	float4 frag(vertexOutput i) : COLOR 
	{
		return i.col;
	}

ENDCG
		}
	}
}