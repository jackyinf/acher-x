﻿using Assets._Mine.Scripts.Helpers;
using UnityEngine;
// ReSharper disable UnusedMember.Local

public class PlayerProjectileManager : MonoBehaviour
{
    //private List<ProjectileDragging> projectiles = new List<ProjectileDragging>(); 
    public GameObject projectileTemplate;
    public Transform projectileSpawnPoistion;
    public GameObject projectileParent;

    private GameObject activeProjectile;
    private float projectileTimeout = 2f;
    private float projectileTimeoutElapsed = 0;
    private bool canSpawnProjectile = false;

	void Start ()
    {
	    Guard.NotNull(projectileTemplate, "projectileTemplate is not set.");
        projectileTemplate.SetActive(false);

        Guard.NotNull(projectileSpawnPoistion, "projectileSpawnPoistion is not set.");
	    Guard.NotNull(projectileParent, "projectileParent is not set.");
    }

    void FixedUpdate ()
    {
	    if (!canSpawnProjectile)
	    {
            // Wait, till we can spawn a projectile
	        projectileTimeoutElapsed += Time.deltaTime;
	        if (!(projectileTimeout <= projectileTimeoutElapsed))
                return;

	        canSpawnProjectile = true;  // Now we can spawn a projectile

            // We spawn it right away
	        SpawnProjectileAndListen();
	    }
	}

    /// <summary>
    /// Instantiates a game object onto the game field and listens, if the projectile was fired or not.
    /// </summary>
    private void SpawnProjectileAndListen()
    {
        // Instantiate
        activeProjectile = Instantiate(projectileTemplate, projectileSpawnPoistion.position, Quaternion.identity) as GameObject;
        activeProjectile.transform.parent = projectileParent.transform;

        var projectileDraggingScript = activeProjectile.GetComponent<ProjectileDragging>(); // we assume that spawned object is projectile
        if (projectileDraggingScript == null)
        {
            Debug.LogError("Spawned projectile does not have \"ProjectileDragging\" script attached to it.");
            return;
        }
        var playersRigidBody = projectileParent.GetComponent<Rigidbody2D>();
        var lineRenderer = projectileParent.GetComponent<LineRenderer>();
        projectileDraggingScript.Connect(playersRigidBody, lineRenderer, lineRenderer);

        activeProjectile.SetActive(true);

        // Listen
        projectileDraggingScript.OnFired += (sender, args) =>
        {
            canSpawnProjectile = false;
            projectileTimeoutElapsed = 0;
        };
    }
}
