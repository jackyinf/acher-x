﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public enum ExplosionType
{
    ParticleSystem = 10,
    SpriteSheet = 20
}

public class ProjecttileDamage : MonoBehaviour
{
    public ParticleSystem explosion;
    public GameObject explosionSprite;
    public ExplosionType type = ExplosionType.SpriteSheet;

    // Use this for initialization
    void Start()
    {
    }

    //void OnCollisionEnter2D(Collider2D other)
    //{
    //    if (other.CompareTag("Enemy"))
    //    {
    //        Debug.Log("bullet entered monster");
    //    }
    //}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            var monster = coll.gameObject.GetComponent<Monster>();
            monster.TakeDamage();

            if (type == ExplosionType.SpriteSheet)
            {
                Instantiate(explosionSprite, transform.position, Quaternion.identity);
            }
            else
            {
                var explosionInstance = Instantiate(explosion, transform.position, Quaternion.identity);
                Destroy(explosionInstance, 1);
            }

            this.gameObject.SetActive(false);

        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}