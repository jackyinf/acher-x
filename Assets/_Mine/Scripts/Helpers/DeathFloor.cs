﻿using Assets._Mine.Scripts.level_d;
using UnityEngine;

public class DeathFloor : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            var player = other.GetComponent<LevelD_PlayerD>();
            if (player)
                player.TakeDamage(99999999);
        }
        else
        {
            Destroy(other);
        }
    }
}
