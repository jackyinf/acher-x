﻿using UnityEngine;
using System.Collections;

public class EnablerDisabler : MonoBehaviour
{

    public bool isEnabled = true;

	// Use this for initialization
	void Awake ()
	{
	    foreach (Transform child in gameObject.transform)
	        child.gameObject.SetActive(isEnabled);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
