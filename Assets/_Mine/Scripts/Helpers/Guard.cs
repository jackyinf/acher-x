﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets._Mine.Scripts.Helpers
{
    public static class Guard
    {
        public static void NotNull(object value, string argument)
        {
            if (value == null)
                throw new ArgumentNullException(argument);
        }

        public static void NotNullOrEmpty<T>(this List<T> list, string argument)
        {
            if (list == null || !list.Any())
                throw new NotNullOrEmptyException();
        }
    }
}