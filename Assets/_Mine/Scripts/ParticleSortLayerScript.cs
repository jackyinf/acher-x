﻿using UnityEngine;

public class ParticleSortLayerScript : MonoBehaviour
{
    public int orderInLayer = 2;
    
	void Start ()
    {
        var renderer = GetComponent<ParticleSystem>().GetComponent<Renderer>();
        renderer.sortingLayerName = "Foreground";
	    renderer.sortingOrder = orderInLayer;
    }
}
