﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets._Mine.Scripts.level_d
{
    [Serializable]
    public enum D_Levels
    {
        level_d,
        level_e
    }

    public class LevelD_NextLevel : MonoBehaviour
    {
        public D_Levels nextLevelName;
    
        public void OnTriggerEnter2D(Collider2D other)
        {

            if (other.gameObject.CompareTag(LDC.Tags.PLAYER))
            {
                SceneManager.LoadScene(nextLevelName.ToString());
            }
        }
    }
}