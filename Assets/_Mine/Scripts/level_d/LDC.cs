﻿namespace Assets._Mine.Scripts.level_d
{
    /// <summary>
    /// LevelD_Constants - LDC
    /// </summary>
    public static class LDC
    {
        public static class Tags
        {
            public const string PLAYER = "Player";
            public const string ENEMY = "Enemy";
        }
    }
}
