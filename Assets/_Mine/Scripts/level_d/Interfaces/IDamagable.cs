﻿namespace Assets._Mine.Scripts.level_d.Interfaces
{
    public interface IDamagable
    {
        void TakeDamage(float damage);
    }
}