﻿using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_WeaponD : MonoBehaviour {

        public GameObject bulletPrefab;
        public float cooldownResetTimeout = .5f;
        public bool isFacedRight;

        private float cooldownElapsed;

        public void Update()
        {
            cooldownElapsed += Time.deltaTime;
        }

        public void Shoot()
        {
            if (cooldownElapsed < cooldownResetTimeout)
                return; // we cannot fire a projectile yet

            // reset timeout
            cooldownElapsed = 0f;

            // shoot a bullet
            var bulletInstance = Instantiate(bulletPrefab, transform.position, bulletPrefab.transform.rotation) as GameObject;
            bulletInstance.GetComponent<LevelD_BulletD>().IsFacedRight = isFacedRight;
            Destroy(bulletInstance, 1);
        }
    }
}
