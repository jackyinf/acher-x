﻿using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_PlayerWeapon : LevelD_WeaponD
    {
        public void UpdateInput()
        {
            if (Input.GetKey(KeyCode.C))
                Shoot();
        }
    }
}
