﻿using Assets._Mine.Scripts.level_d.Interfaces;
using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_EnemyD : MonoBehaviour, IDamagable
    {
        public LayerMask playerMask;
        public bool IsFacedRight;

        private LevelD_EnemyWeapon _levelDEnemyWeapon;

        public void Start()
        {
            _levelDEnemyWeapon = GetComponentInChildren<LevelD_EnemyWeapon>();
        }

        public void FixedUpdate()
        {
            // look for a player
            RaycastHit2D hit = Physics2D.Raycast(transform.position, IsFacedRight ? Vector2.right : Vector2.left, 10f, playerMask);
            if (!hit)
                return; // player not found

            if (_levelDEnemyWeapon) // has weapon to shoot with
                _levelDEnemyWeapon.Shoot();
        }

        public void TakeDamage(float damage)
        {
            Destroy(gameObject);
        }
    }
}
