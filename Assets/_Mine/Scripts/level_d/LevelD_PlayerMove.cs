﻿using System;
using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_PlayerMove : MonoBehaviour
    {
        public float speed = 20;
        public float jumpForce = 5;
        public float distanceToGround = 0.22f;  // half of the scale of the player +0.02 tolerance
        public LayerMask groundMask;
        public float fallPower = 0.1f;

        private float _move;
        private bool _jump;
        private Rigidbody2D _rb;
        private float _fall;
        private float _fallTolerance = -0.1f;
        private RaycastHit2D _onGround;

        public void UpdateInput ()
        {
            _move = Input.GetAxis("Horizontal") * speed;
            _fall = Mathf.Min(Input.GetAxis("Vertical") * fallPower, 0);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Update: jump is down");
                _jump = true;
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                Debug.Log("Update: jump released");
                _jump = false;
            }
        }

        public void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();

            if (groundMask == 0)
                throw new Exception("Ground mask is not set");
        }

        public void FixedUpdate()
        {
            _onGround = Physics2D.Raycast(transform.position, -Vector2.up, distanceToGround, groundMask);

            Move();

            Jump();
        }

        public void Move()
        {
            transform.Translate(_move, _onGround ? 0 : _fall, 0);

            //// Ice skating simulation
            //_rb.AddForce(new Vector2(_move, 0));
        }

        public void Jump()
        {

            //if (IsFalling())
            //    return;

            if (!_jump)
                return;

            if (!_onGround)
            {
                // slow ascend. Luke taught how to use force
                _rb.AddForce(Vector2.up * 2, ForceMode2D.Force);
                return;
            }

            // LEAP!.. because of the implulse
            Debug.Log("Jump: jumped");
            _rb.AddForce(Vector2.up*jumpForce, ForceMode2D.Impulse);
        }

        private bool IsFalling()
        {
            return _fall < _fallTolerance;
        }
    }
}
