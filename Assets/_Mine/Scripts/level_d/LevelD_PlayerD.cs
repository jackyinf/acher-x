﻿using Assets._Mine.Scripts.level_d.Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_PlayerD : MonoBehaviour, IDamagable
    {
        private LevelD_PlayerMove _levelDPlayerMove;
        private LevelD_PlayerWeapon _levelDPlayerWeapon;

        public void Awake()
        {
            _levelDPlayerMove = GetComponent<LevelD_PlayerMove>();
            _levelDPlayerWeapon = GetComponentInChildren<LevelD_PlayerWeapon>();
        }

        public void Update()
        {
            _levelDPlayerMove.UpdateInput();
        }

        public void FixedUpdate ()
        {
            _levelDPlayerMove.Move();
            _levelDPlayerMove.Jump();
            _levelDPlayerWeapon.UpdateInput();
        }

        public void TakeDamage(float damage)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
