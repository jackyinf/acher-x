﻿using System.Collections.Generic;
using Assets._Mine.Scripts.Helpers;
using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_BlockMove : MonoBehaviour
    {
        public List<Transform> waypoints = new List<Transform>();
        public float moveSpeed = 0.1f;
        public float pointRadius = 0f;
        public float smoothTime = 1;
    
        private Vector2 previousDestinationPoint;
        private Vector2 nextDestinationPoint;
        private int nextDestinationIndex;
        private Vector3 currentMoveDirectionNormalized;

        private Vector2 currentVelocity = Vector2.zero;

        void Start ()
        {
            waypoints.NotNullOrEmpty("At least one point should exist");

            nextDestinationIndex = 0;
            SetNewDestination(nextDestinationIndex);
        }
	
        public void FixedUpdateChild ()
        {
            Move();

            // Check if made it 
            Collider2D destinationCollider = Physics2D.OverlapCircle(nextDestinationPoint, pointRadius);
            if (destinationCollider)
                SetNewDestination(++nextDestinationIndex);
        }

        private void Move()
        {
            //transform.position = new Vector2(transform.position.x + currentMoveDirectionNormalized.x, transform.position.y + currentMoveDirectionNormalized.y);

            //transform.position = transform.position + currentMoveDirectionNormalized * moveSpeed;

            //transform.position = Vector2.Lerp(new Vector2(transform.position.x, transform.position.y), nextDestinationPoint, 0.01f);
            //currentVelocity = currentVelocity.normalized * moveSpeed;
            transform.position = Vector2.SmoothDamp(transform.position, nextDestinationPoint, ref currentVelocity, smoothTime);
        }

        private void SetNewDestination(int index)
        {
            previousDestinationPoint = transform.position;

            var nextIndex = index == -1 ? 0 : index%waypoints.Count;
            var nextDestinationTransform = waypoints[nextIndex];  // take next item in list 
            nextDestinationPoint = new Vector2(nextDestinationTransform.position.x, nextDestinationTransform.position.y); 
            var currentMoveDirection = nextDestinationPoint - previousDestinationPoint;
            currentMoveDirectionNormalized = currentMoveDirection.normalized;
            Debug.Log("Setting new destination to " + nextDestinationPoint);
        }
    }
}
