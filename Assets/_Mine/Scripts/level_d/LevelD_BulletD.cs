﻿using Assets._Mine.Scripts.level_d.Interfaces;
using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public abstract class LevelD_BulletD : MonoBehaviour
    {
        public float bulletSpeed = 0.3f;
        public bool IsFacedRight;
        public float bulletDamage = 50f;

        public abstract string TargetTag { get; }

        public void FixedUpdate()
        {
            transform.position = new Vector3(transform.position.x + (IsFacedRight ? bulletSpeed : -bulletSpeed), transform.position.y, transform.position.z);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag(TargetTag))
            {
                var damagables = other.gameObject.GetComponents<IDamagable>();
                foreach (var damagable in damagables)
                    damagable.TakeDamage(bulletDamage);

                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
