﻿using UnityEngine;

namespace Assets._Mine.Scripts.level_d
{
    public class LevelD_Block : MonoBehaviour
    {
        private LevelD_BlockMove _levelDBlockMove;

        // Use this for initialization
        void Start ()
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.material.color = Color.blue;

            _levelDBlockMove = GetComponent<LevelD_BlockMove>();
        }

        void FixedUpdate()
        {
            if (_levelDBlockMove)
                _levelDBlockMove.FixedUpdateChild();
        }
    }
}
