﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Random = System.Random;
// ReSharper disable UnusedMember.Local

public class MonsterSpawner : MonoBehaviour
{
    public List<Transform> spawnPoints;
    public List<GameObject> enemies;

    private readonly Random random = new Random();

	void Start ()
	{
	    spawnPoints = spawnPoints ?? new List<Transform>();
	    enemies = enemies ?? new List<GameObject>();

	    StartCoroutine(MonsterSpawningCoroutine());
	}

    IEnumerator MonsterSpawningCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            SpawnMonster();
        }
    }

    private void SpawnMonster()
    {
        if (!spawnPoints.Any())
        {
            Debug.Log("Nowhere to spawn");
            return;
        }

        if (!enemies.Any())
        {
            Debug.Log("No-one to spawn");
            return;
        }

        var spawnPoint = spawnPoints[random.Next(0, spawnPoints.Count)];
        var enemy = enemies[random.Next(0, enemies.Count)];

        Instantiate(enemy, spawnPoint.position, Quaternion.identity);
    }
}
