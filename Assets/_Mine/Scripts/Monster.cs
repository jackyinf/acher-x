﻿using UnityEngine;
using System.Collections;
// ReSharper disable UnusedMember.Local

public class Monster : MonoBehaviour
{
    public float totalHP = 100;
    private MonsterController _monsterController;

    void Awake()
    {
        _monsterController = GetComponent<MonsterController>();
    }

    public void TakeDamage(int damage = 50)
    {
        totalHP -= damage;
        _monsterController.SlowDownBy(0.5f);
        Debug.Log("bullet entered monster " + totalHP);
        if (totalHP <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        this.gameObject.SetActive(false);

    }
}
