﻿using System.Collections;
using UnityEngine;
// ReSharper disable UnusedMember.Local

public class MonsterController : MonoBehaviour
{
    private const float DEFAULT_SPEED_MULTIPLIER = 1f;

    public float speed = -0.05f;
    public float speedMultiplier = DEFAULT_SPEED_MULTIPLIER;

    void FixedUpdate()
    {
        transform.Translate(new Vector2(speed * speedMultiplier, 0));
    }

    public void SlowDownBy(float value = 0.5f)
    {
        speedMultiplier = value;
        StartCoroutine(ResetSpeed());
    }

    IEnumerator ResetSpeed()
    {
        yield return new WaitForSeconds(1);
        speedMultiplier = DEFAULT_SPEED_MULTIPLIER;
    }
}
