﻿using UnityEngine;
using System.Collections;

public class DeadAfterSeconds : MonoBehaviour
{

    public float manualLength = 0.5f;

	void Start ()
	{
	    if (manualLength <= 0)
	    {
            manualLength = GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).LongLength;
        }
        Debug.Log(manualLength);
	    Destroy(gameObject, manualLength);
	}
}
